package com.joeqiang.cloudbook.controller;

import com.joeqiang.cloudbook.exception.JsonResult;
import com.joeqiang.cloudbook.mapper.LoginMapper;
import com.joeqiang.cloudbook.pojo.User;
import com.joeqiang.cloudbook.service.RegistService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 注册
 */
@Controller
public class RegistController {
    @Autowired
    RegistService registService;

    @RequestMapping("regist")
    @ResponseBody
    public JsonResult regist(String name, String password, String nick, String confirm) {

       return  new JsonResult(registService.regist(name, password, nick));
    }

}
