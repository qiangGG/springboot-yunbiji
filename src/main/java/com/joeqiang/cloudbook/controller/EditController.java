package com.joeqiang.cloudbook.controller;

import com.joeqiang.cloudbook.exception.JsonResult;
import com.joeqiang.cloudbook.service.EditService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class EditController {
    @Autowired
    EditService editService;

    //保存笔记
    @RequestMapping("/saveNote.do")
    @ResponseBody
    public JsonResult edit(int userId, String title, String body, int notebookId) {
        System.out.println("标题:" + title);
        System.out.println("内容:" + body);
        System.out.println("用户iD:" + userId);
        System.out.println("notebookId:" + notebookId);

        return new JsonResult(editService.saveNote(notebookId, userId, title, body));
    }

    //获取笔记本
    @RequestMapping("/getNote.do")
    @ResponseBody
    public JsonResult getNode(int userId) {
        System.out.println("获取笔记本");
        return new JsonResult(editService.getNote(userId));
    }

    //创建笔记本
    @RequestMapping("/createNote.do")
    @ResponseBody
    public JsonResult createNote(int userId, String noteName) {
        System.out.println("创建笔记本");
        System.out.println(userId);
        System.out.println(noteName);
        return new JsonResult(editService.createNote(userId, noteName));
    }

    //查询笔记本下的笔记
    @RequestMapping("/findNote.do")
    @ResponseBody
    public JsonResult findNote(int notebookId) {
        System.out.println("查询笔记本下的笔记");
        return new JsonResult(editService.findNote(notebookId));
    }

    //获取笔记内容
    @RequestMapping("getCloudNote.do")
    @ResponseBody
    public JsonResult getCloudNote(int noteId) {
        System.out.println("获取笔记内容");
        return new JsonResult(editService.getCloudNote(noteId));
    }

    //更新笔记内容
    @RequestMapping("updateNote.do")
    @ResponseBody
    public JsonResult updateNote(String body, String title, int noteId) {
        System.out.println("更新笔记内容");
        return new JsonResult(editService.updateNote(body, title, noteId));
    }

    //修改笔记标题
    @RequestMapping("renameNote.do")
    @ResponseBody
    public JsonResult renameNote(String title, String body, int noteId) {

        System.out.println("修改标题操作");
        System.out.println(title);
        System.out.println(body);
        System.out.println(noteId);

        return new JsonResult(editService.renameNote(title, noteId));
    }

    //逻辑删除
    @RequestMapping("deleteNote.do")
    @ResponseBody
    public JsonResult deleteNote(String noteId) {
        System.out.println("逻辑删除:"+noteId);
        return new JsonResult(editService.deleteNote(noteId));
    }
    //新建笔记
    @RequestMapping("createNewNote.do")
    @ResponseBody
    public JsonResult createNewNote(int notebookId,int userId,String title){
        System.out.println("新建笔记");
        System.out.println("title :"+title);
        System.out.println("notebookId :"+notebookId);

        return new JsonResult(editService.createNewNote(notebookId,userId,title));
    }
    //删除笔记本
    @RequestMapping("del.do")
    @ResponseBody
    public  JsonResult del(int notebookId){
        System.out.println("删除笔记本");
        System.out.println("要删除笔记本Id:"+notebookId);
        return new JsonResult(editService.delNotebook(notebookId));
    }
}
