package com.joeqiang.cloudbook.controller;
import com.joeqiang.cloudbook.exception.JsonResult;
import com.joeqiang.cloudbook.mapper.LoginMapper;
import com.joeqiang.cloudbook.pojo.User;
import com.joeqiang.cloudbook.service.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import javax.servlet.http.HttpSession;


/**
 * 登陆
 */


@Controller
public class LoginController {
    @Autowired
    LoginService loginService;
    @Autowired
    LoginMapper loginMapper;
    @RequestMapping("/login")
    public String login() {
        return "login";
    }

    @RequestMapping("/login.do")
    @ResponseBody
    public JsonResult  login(String name, String password , HttpSession session) {
        User user = loginMapper.login(name, password);
       session.setAttribute("user",user);
        return new JsonResult(loginService.login(name, password));
    }
    @RequestMapping("/edit")
    public String edit(){

        return "edit";
    }
    @RequestMapping("/a")
    public  String a(){
        return "activity_detail";
    }
    @RequestMapping("/activity")
    public  String activity(){
      return "activity";
    }
    @RequestMapping("Change_password")
    public  String Change_password(){
        return "Change_password";

     }

    //修改密码
    @RequestMapping("/changepwd.do")
    @ResponseBody
    public JsonResult changepwd(int newpassword,int userId){
        System.out.println("修改密码");
        System.out.println("用户id:"+userId);
        System.out.println("newpassword::"+newpassword);
        return new JsonResult(loginService.changepwd(newpassword,userId));

    }
}
