//package com.joeqiang.cloudbook.aop;
//import org.aspectj.lang.annotation.After;
//import org.aspectj.lang.annotation.AfterReturning;
//import org.aspectj.lang.annotation.AfterThrowing;
//import org.aspectj.lang.annotation.Aspect;
//import org.aspectj.lang.annotation.Before;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.stereotype.Component;
///**
// * 创建一个切面组件,就是一个普通的JavaBean
// *
// *
// * **/
//@Component
//@Aspect
//public class DemoAspect {
//    //申明test方法将在loginService的全部方法之前运行
////    @Before("bean(loginService)")
////    public void test(){
////        System.out.println("调用loginService方法");
////    }
////    @After("bean(loginService)")
////    public void test1(){
////        System.out.println("在调用loginService方法前调用");
////    }
//    @AfterReturning("bean(loginService)")
//    public void test3(){
//        System.out.println("AfterReturning");
//    }
//    @AfterThrowing("bean(loginService)")
//    public void test4(){
//        System.out.println("AfterThrowing");
//    }
///**
// * 环绕通知方法:
// * 1.必须有返回值Object
// * 2.必须有参数ProceedingJoinPoint
// * 3.需要在方法中调用 jp.proceed()
// * 4.必须抛出异常
// * 5.返回业务方法的返回值
// * @param jp
// * @return
// * @throws Throwable
// */
////  @Around("bean(userService)")
////  public Object test5(ProceedingJoinPoint jp)throws Throwable{
////	  Object val=jp.proceed();
////	  System.out.println("业务结果:"+val);
////	  throw new UserNotFoundException("就是不让登录") ;
////
////
////
// }
