package com.joeqiang.cloudbook.aop;

import com.joeqiang.cloudbook.pojo.Logger;
import com.joeqiang.cloudbook.pojo.User;
import org.aopalliance.intercept.Joinpoint;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.*;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;


/**
 * 方法切入点(execution：执行)
 * execution(修饰词   类名.方法名(参数类型))
 * execution(* cn.tedu.note.service.UserService.login(...))
 * Component （把普通pojo实例化到spring容器中，相当于配置文件中的<bean id="" class=""/>）
 */
@Component
@Aspect
public class PointcutAspect {
    //匹配com.joeqiang.cloudbook.controller包及其子包下的所有类的所有方法

    @After("bean(*Service)")
    public void deBefore(JoinPoint jp) throws Throwable {
        System.out.println("******************执行后置通知******************");
        // 接收到请求，记录请求内容
        Long startTimeMillis = System.currentTimeMillis();
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        System.out.println("URL :" + request.getRequestURI());
        System.out.println("HTTP_METHOD :" + request.getMethod());
        // System.out.println("IP:" + request.getRemoteAddr());
        //用的最多 通知的签名
        Signature signature = jp.getSignature();
        System.out.println("CLASS_METHOD :" + jp.getStaticPart());
        System.out.println("ARGS:" + Arrays.toString(jp.getArgs()));
        //代理的是哪一个方法
        System.out.println("代理方法名 :" + signature.getName());
        //AOP代理类的名字
        System.out.println("代理类名字:" + signature.getDeclaringTypeName());
        //AOP代理类的类（class）信息
        System.out.println("AOP代理类信息:" + signature.getDeclaringType());
        //获取RequestAttributes
        RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
        //如果要获取Session信息的话，可以这样写：
        HttpSession session = (HttpSession) requestAttributes.resolveReference(RequestAttributes.REFERENCE_SESSION);
        User user = (User) session.getAttribute("user");
        Long endTimeMillis = System.currentTimeMillis();
        //格式化开始时间
        String startTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(startTimeMillis);
        //格式化结束时间
        String endTime = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(endTimeMillis);
        if (user != null) {
            System.out.println("Session信息 :" + user.getName());

            System.out.println("操作人 : " + user.getName() + "操作方法 : " + signature.getName()
                    + " 开始时间 : " + startTime + " 结束时间 : " + endTime);
        }


    }


//    @Before("execution(* com.joeqiang.cloudbook.controller.LoginController.login())")
//    public void after() {
//
//
//            throw new RuntimeException("不给登陆");
//
//
//    }

    @AfterReturning(value = "execution( * com.joeqiang.cloudbook.controller..*.*(..))", returning = "keys", argNames = "keys")

    public void doAfterReturningAdvice2(String keys) {
        System.out.println("******************后置返回通知******************");
        System.out.println("后置返回通知的返回值：" + keys);
    }

    //捕获 Service 发生的异常信息
    @AfterThrowing(value = "bean(*Controller)", throwing = "exception")
    public void doAfterThrowingAdvice(JoinPoint joinPoint, Throwable exception) {
        System.out.println("******************异常通知******************");
        // /目标方法名：
        System.out.println("方法名:" + joinPoint.getSignature().getName());

        System.out.println("异常信息:" + exception.getMessage());

    }

}

