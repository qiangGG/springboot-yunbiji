package com.joeqiang.cloudbook.aop;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;
/*
 *对软件的业务层进行性能测试
 * * 环绕通知方法:
* 1.必须有返回值Object
 * 2.必须有参数ProceedingJoinPoint
 * 3.需要在方法中调用 jp.proceed()
 * 4.必须抛出异常
 * 5.返回业务方法的返回值
 */
@Component
@Aspect
public class TimeAspect {
    @Around("bean(*Service)")
	public Object test(ProceedingJoinPoint jp) throws Throwable {
		System.out.println("****************执行环绕通知******************");
		long t1 = System.currentTimeMillis();
		Object val = jp.proceed();//目标方法
		long t2 = System.currentTimeMillis();
		long t = t2 - t1;
		//JoinPoint 对象可以获取目标业务方法的详细信息:方法签名,调用参数
		Signature m = jp.getSignature();
		//方法名
		String name = m.getName();
		//Sinnature:签名:这里是方法签名
		System.out.println("执行方法 :"+name + "总共用时:" + t+"毫秒");
		return val;
	}
}
