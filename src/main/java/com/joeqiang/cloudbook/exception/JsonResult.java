package com.joeqiang.cloudbook.exception;

import java.io.Serializable;

public class JsonResult implements Serializable {
    private static final int SUCCESS = 0;
    private static final int ERROR = 1;
    private int state;
    private String message;
    private Object  data;

    public JsonResult() {
    }

    public JsonResult(String error) {
        state = ERROR;
        this.message = error;
    }

    public JsonResult(Throwable e) {
        state = 1;
        message = e.getMessage();

    }
    public  JsonResult(Object data){
          state =SUCCESS;
          this.data=data;
    }
    public JsonResult(int state,String e) {
        this.state=state;
        this.message=e;
    }
    public JsonResult(int state, String message, Object data) {

        this.state = state;
        this.message = message;
        this.data = data;
    }
    public int getState() {
        return state;
    }
    public void setState(int state) {
        this.state = state;
    }
    public String getMessage() {
        return message;
    }
    public void setMessage(String message) {
        this.message = message;
    }
    public Object getData() {
        return data;
    }
    public void setData(Object data) {
        this.data = data;
    }
}
