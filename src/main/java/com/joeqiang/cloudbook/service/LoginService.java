package com.joeqiang.cloudbook.service;

import com.joeqiang.cloudbook.exception.JsonResult;
import com.joeqiang.cloudbook.mapper.LoginMapper;
import com.joeqiang.cloudbook.pojo.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;

@Service
public class LoginService {
    @Autowired
    LoginMapper loginMapper;

    public JsonResult login(String name, String password) {

        User user = loginMapper.login(name, password);
        if (user != null) {
            return new JsonResult(user);
        }
        return new JsonResult(1, "用户名或密码错误");
    }

    public JsonResult changepwd(int newpassword,int userId) {
        Integer changepwd = loginMapper.changepwd(newpassword,userId);
        if (changepwd >0){
            return new JsonResult(changepwd);
        }
        return new JsonResult(1,"修改密码失败");
    }
}
