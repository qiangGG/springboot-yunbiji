package com.joeqiang.cloudbook.service;

import com.joeqiang.cloudbook.exception.JsonResult;
import com.joeqiang.cloudbook.mapper.EditMapper;
import com.joeqiang.cloudbook.pojo.Note;
import com.joeqiang.cloudbook.pojo.Notebook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@Service
public class EditService {
    @Autowired
    EditMapper editMapper;

    public JsonResult saveNote(int notebookId, int userId, String title, String body) {

        SimpleDateFormat slf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String createtime = slf.format(new Date().getTime());
        Integer saveNote = editMapper.saveNote(notebookId, userId, title, body, createtime);
        return new JsonResult(saveNote);
    }

    public JsonResult getNote(int userId) {
        List<Notebook> note = editMapper.getNote(userId);
        return new JsonResult(note);
    }

    public JsonResult createNote(int userId, String noteName) {
        SimpleDateFormat slm = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String createtime = slm.format(new Date().getTime());
        Integer note = editMapper.createNote(userId, noteName, createtime);
        if (note > 0) {
            return new JsonResult(note);
        }
        return new JsonResult(1, "创建失败");
    }

    public JsonResult findNote(int notebookId) {
        List<Note> noteName = editMapper.finfNote(notebookId);
        if (noteName.size() > 0) {
            return new JsonResult(noteName);
        }
        return new JsonResult(1, "该笔记本下没有笔记");
    }

    public JsonResult getCloudNote(int noteId) {
        Note cloudNote = editMapper.getCloudNote(noteId);
        if (cloudNote != null) {
            return new JsonResult(cloudNote);
        }
        return new JsonResult(1, "请增加笔记内容");
    }

    public JsonResult updateNote(String body, String title, int noteId) {
        Integer updateNote = editMapper.updateNote(body, title, noteId);
        if (updateNote > 0) {
            return new JsonResult(updateNote);
        }
        return new JsonResult(1, "保存失败");
    }

    public JsonResult renameNote(String title, int noteId) {
        Integer renameNote = editMapper.renameNote(title, noteId);
        if (renameNote > 0) {
            return new JsonResult(renameNote);
        }
        return new JsonResult(1, "修改标题失败");
    }

    public JsonResult deleteNote(String noteId) {
        Integer deleteNote = editMapper.deleteNote(noteId);
        if(deleteNote>0){
            return new JsonResult(deleteNote);
        }
        return new JsonResult(1,"删除失败");
    }

    public JsonResult createNewNote(int notebookId,int userId,String title) {
        SimpleDateFormat slf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        String createtime = slf.format(new Date().getTime());
        Integer newNote = editMapper.createNewNote(notebookId, userId, title, createtime);
        if (newNote>0){
            return new JsonResult(newNote);
        }
       return new JsonResult(1,"创建笔记失败");
    }

    public JsonResult delNotebook(int notebookId) {
        Integer delNotebook = editMapper.delNotebook(notebookId);
        List<Note> notes = editMapper.finfNote(notebookId);
        if (notes.size()>0){
            editMapper.delNote(notebookId);
        }
        if (delNotebook>0){
            return new JsonResult(delNotebook);
        }

      return new JsonResult(1,"删除失败");
    }
}
