package com.joeqiang.cloudbook.service;

import com.joeqiang.cloudbook.exception.JsonResult;
import com.joeqiang.cloudbook.mapper.LoginMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RegistService {
    @Autowired
    LoginMapper loginMapper;
   public JsonResult regist(String name,String password,String nick){
       Integer checkName = loginMapper.checkName(name);
       if (checkName>0){
           return new JsonResult(1,"用户名已存在");
       }
       Integer b = loginMapper.regist(name, password, nick);
       return new JsonResult(b);
   }
}
