package com.joeqiang.cloudbook.mapper;

import com.joeqiang.cloudbook.pojo.Note;
import com.joeqiang.cloudbook.pojo.Notebook;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface EditMapper {
    @Insert("INSERT INTO cn_note (cn_notebook_id,cn_user_id,cn_note_title,cn_note_body,cn_note_create_time)" +
            "VALUES(#{notebookId},#{userId},#{title},#{body},#{createtime})")
    Integer saveNote(@Param("notebookId") int notebookId,@Param("userId") int userId, @Param("title") String title, @Param("body")
            String body, @Param("createtime") String createtime);

    @Select("select cn_notebook_id as id,cn_notebook_name as name from cn_notebook where cn_userId =#{userId}")
    List<Notebook> getNote(int userId);

    @Insert("INSERT INTO cn_notebook(cn_userId,cn_notebook_name,cn_createtime) VALUES (#{userId},#{noteName},#{createtime})")

    Integer createNote(@Param("userId") int userId,@Param("noteName") String noteName,@Param("createtime") String createtime);

    @Select("select cn_note_id as id, cn_note_title  as title from cn_note where cn_notebook_id =#{notebookId} AND cn_note_state=0")
    List<Note>  finfNote(int notebookId);

    @Select("select cn_note_id as id,cn_user_id as userId,cn_note_title as title,cn_note_body as body from cn_note where cn_note_id =#{noteId}")
    Note getCloudNote(int noteId);
     @Update("UPDATE cn_note SET cn_note_body =#{body},cn_note_title =#{title} where cn_note_id =#{noteId}")
    Integer updateNote(@Param("body") String body,@Param("title") String title,@Param("noteId") int noteId);

    @Update("UPDATE cn_note SET cn_note_title =#{title} WHERE  cn_note_id =#{noteId} ")
    Integer renameNote(@Param("title") String title,@Param("noteId") int noteId);

    @Update("UPDATE cn_note SET cn_note_state =1 where cn_note_id =#{noteId}")
    Integer deleteNote(String noteId);

    @Insert("INSERT INTO cn_note (cn_notebook_id,cn_user_id,cn_note_state,cn_note_title,cn_note_create_time) VALUES(#{notebookId},#{userId},0,#{title},#{createtime})")
    Integer createNewNote(@Param("notebookId") int notebookId,@Param("userId") int userId,@Param("title") String title,@Param("createtime") String createtime);

    @Delete("DELETE FROM cn_notebook WHERE cn_notebook_id =#{notebookId}")
    Integer delNotebook(int notebookId);
    @Delete("DELETE FROM cn_note WHERE cn_notebook_id=#{notebookId}")
    Integer delNote(int notebookId);
}
