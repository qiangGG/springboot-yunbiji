package com.joeqiang.cloudbook.mapper;

import com.joeqiang.cloudbook.pojo.User;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface LoginMapper {
    //注册
    @Insert("insert INTO cn_user (name,password,nick)VALUES" +
            "(#{name},#{password},#{nick}); ")
    Integer regist(@Param("name") String name, @Param("password") String password, @Param("nick") String nick);

    @Select("select * from cn_user")
    List<User> findAll();

    @Select("select Count(name) from cn_user where name = #{name}")
    Integer checkName(String name);

    //登陆
    @Select("select * from cn_user where name =#{name} And password=#{password}")
    User login(@Param("name") String name,@Param("password") String password);

    //修改密码
    @Update("update cn_user set password =#{newpassword} where id=#{userId}")
    Integer changepwd(@Param("newpassword") int lastpassword,@Param("userId") int userId);
}
