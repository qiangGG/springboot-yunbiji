package com.joeqiang.cloudbook;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.ServletComponentScan;

@SpringBootApplication
@ServletComponentScan   //用于过滤器配置，不加过滤器失效
public class Application  {
	public static void main(String[] args) {
		System.out.println("Springboot开始启动");
		SpringApplication.run(Application.class, args);
		System.out.println("Springboot启动成功");
	}
}
