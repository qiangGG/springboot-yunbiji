package com.joeqiang.cloudbook.pojo;

public class User {
    private int id;
    private String name;
    private String nick;
    private String password;
    private String confirm;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getconfirm() {
        return confirm;
    }

    public void setconfirm(String confirm) {
        this.confirm = confirm;
    }

    public String getNick() {
        return nick;
    }

    public void setNick(String nick) {
        this.nick = nick;
    }
}


