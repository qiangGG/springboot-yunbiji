//script/note.js 编码为utf-8
var SUCCESS = 0;
var ERROR = 1;
$(function () {
    alert("1111");
    //var userId = getCookie('userId');
    //console.log(userId);
    //网页加载以后立即读取笔记本列表
    loadNotebooks();
    //绑定笔记本列表区域点击事件
    //on()绑定事件可以区别事件源，click()绑定事件无法区别事件源
    $('#notebook-list').on('click', '.notebook', loadNotes);
    //绑定点击事件到笔记note的<li>上,显示笔记内容
    $('#note-list').on('click', '.note', loadNoteContent);
    //给添加笔记按钮设置单机监听事件
    $('#note-list').on('click', '#add_note', showAddNoteDialog);
    //给创建设置点击事件监听
    $('#can').on('click', '.creat-note', addNote);
    //给取消和叉号设置点击事件
    $('#can').on('click', '.close,.cancel', closeDialog);
    //给保存笔记设置时间监听
    $('#save_note').click(saveNote);
    //绑定笔记子菜单的触发事件
    $('#note-list').on('click', '.btn-note-menu', showNoteMenu);
    $(document).click(hideNoteMenu);
    //绑定移动点击事件
    $('#note-list').on('click', '.btn-note-move', showMoveDialog);
    //绑定note删除点击事件
    $('#note-list').on('click', '.btn-note-delete', showDeleteDialog);
});

//显示note删除对话框
function showDeleteDialog() {
    $('#can').load('alert/alert_delete_note.html', function () {
        $('opacity_bg').show();
        console.log("显示删除对话框");
        //给删除按钮绑定单击事件
        $('.sure').click(deleteNote);
    });
}

//删除note
function deleteNote() {
    //关闭删除对话框
    closeDialog();
    //将当前note的状态改为0
    //获取url
    var url = 'note/deleteNote.do';
    //获取UserId
    var noteId = $('#note-list').find('a[class="checked"]').parent().data('noteId');
    var data = {noteId: noteId};
    $.post(url, data, function (result) {
        if (result.state == 0) {
            console.log("删除成功");
            //刷新列表
            $('#notebook-list').find('a[class="checked"]').parent().click();
        } else {
            alset(result.message);
        }
    });

}

//显示移动对话框
function showMoveDialog() {
    $('#can').load('alert/alert_move.html', function () {
        $('opacity_bg').show();
        //加载笔记本列表
        loadNotebooksList();
    });
}

//加载笔记子菜单的移动项笔记本列表
function loadNotebooksList() {
    //var url = 'notebook/list.do';
    var userId = getCookie('userId');
    var data = {userId: userId};
    $.getJSON(url, data, function (result) {
        console.log('loadNotebooksList');
        if (result.state == 0) {
            var data = result.data;
            showNotebooksList(data);
        } else {
            alert(result.message);
        }
    });
}

//显示笔记子菜单的移动项笔记本列表
function showNotebooksList(data) {
    //获取select
    var select = $('#moveSelect');
    //清空select
    select.empty();
    //获取当前笔记本的名称
    var title = $('#notebook-list').find('a[class="checked"]').text();
    console.log(title);
    //显示下拉 菜单
    for (var i = 0; i < data.length; i++) {
        var notebook = data[i];
        if (notebook.name != title) {
            var option = notebooksListTemplate.replace('[notebookName]', notebook.name);
            select.append(option);
        }

    }
    $('.sure').click(moveNote);
}

//移动笔记
function moveNote() {
    //获取笔记本名字
    var title = $('#moveSelect').val();
    console.log(title);
    //关闭窗口
    closeDialog();
    console.log('移动笔记');
    //获取请求路径
    var url = 'note/moveNote.do';
    //获取noteId
    var noteId = $('#note-list').find('a[class="checked"]').parent().data('noteId');
    //获取notebookId
    var notebookId = $(document).data(title);
    var data = {noteId: noteId, notebookId: notebookId};
    $.post(url, data, function (result) {
        console.log("发送移动笔记请求");
        if (result.state == 0) {
            //移动成功
            console.log("移动笔记成功");
            //刷新笔记显示列表
            $('#notebook-list').find('a[class="checked"]').parent().click();
        } else {
            alert(result.message);
        }
    });
}

//笔记子菜单触发事件
function showNoteMenu() {
    console.log('note-menu');
    //找到菜单调方法
    var btn = $(this);
    //只有被选定的当前笔记项目才可以调出子菜单
    //btn.parent('.checked')获取当前按钮的父元素，在父元素满足条件的情况下
    //才返回Jquery对象，调用Jquery的方法，否则就返回空的Jquery元素。
    btn.parent('.checked').next().toggle();
    //阻止点击事件的继续传播，在方法中返回false
    return false;

}

function hideNoteMenu() {
    $('.note_menu').hide();
}

var notebooksListTemplate = '<option >[notebookName]</option>';

//保存笔记
function saveNote() {
    alert("saveNote")
    console.log('saveNote');
    //获取请求路径
    var url = 'saveNote.do';
    //获取noteId
    var noteId = $('#note-list').find('a[class="checked"]').parent().data('noteId');
    //获取title
    var title = $('#input_note_title').val();
    //获取body
    var body = um.getContent();
    console.log(body)
    //获取旧版note
    var note = $(document).data('note');
    console.log(note);
    //比较新旧note有哪些变化，将有改变的值发送给服务器
    var data = {};
    data.noteId = noteId;
    if (title == note.title && body == note.body) {
        console.log('没有改变，不需要发送请求');
        return;
    }
    if (title != note.title) {
        data.title = title;
    }
    if (body != note.body) {
        data.body = body;
    }
    //发送请求
    console.log(data);
    $.post(url, data, function (result) {
        if (result.state == 0) {
            //保存笔记成功,显示保存成功
            console.log("保存笔记成功");
            //刷新笔记列表
            var li = $('#notebook-list').find('a[class="checked"]').parent();
            li.click();
        } else {
            alert(result.message);
        }
    });
}

//关闭新建笔记窗口
function closeDialog() {
    $('.opacity_bg').hide();
    $('#can').empty(); //从元素中移除内容
}

//添加笔记
function addNote() {
    console.log('addNote');
    var url = 'note/addNote.do';
    //获取标题
    var title = $('#input_note').val();
    console.log(title);
    //获取userId
    var userId = getCookie('userId');
    console.log(userId);
    //获取notebookId
    var notebookId = $('#notebook-list').find('a[class="checked"]').parent().data('notebookId');
    console.log(notebookId);
    //发送ajax请求,刷新笔记列表
    var data = {title: title, userId: userId, notebookId: notebookId};
    $.post(url, data, function (result) {
        console.log(result);
        if (result.state == 0) {
            //新建笔记成功
            //关闭新建笔记的窗口
            closeDialog();
            //刷新笔记列表
            var li = $('#notebook-list').find('a[class="checked"]').parent();
            li.click();
            //默认新建笔记单击  线程异步导致下面代码不能选中新建的项目
            /*$('#note-list').find('.note').eq(0).click();*/
        } else {
            alert(result.message);
        }

    });
}

//打开新建笔记窗口
function showAddNoteDialog() {
    $('#can').load('alert/alert_note.html', function () {
        $('.opacity_bg').show();
        $('#input_note').focus();
    });
}

//加载笔记内容
function loadNoteContent() {
    console.log('loadNoteContent');
    //获取当前li
    var li = $(this);
    //清楚之前的默认选中项
    li.parent().find('a').removeClass('checked');
    //添加默认选中
    li.find('a').addClass('checked');
    //获取noteId
    var data = {noteId: li.data('noteId')};
    console.log(data);
    //获取请求路径
    var url = "note/loadNote.do"
    //发起ajax请求
    $.getJSON(url, data, function (result) {
        if (result.state == SUCCESS) {
            var note = result.data;
            //将笔记添加到document
            $(document).data('note', note);
            //显示笔记标题
            $('#input_note_title').val(note.title);
            //显示笔记内容
            um.setContent(note.body);
        } else {
            alert(result.message);
        }
    });

}

//加载笔记Note
function loadNotes() {
    var url = "getNote.do";
    //获取笔记本的id(notebookId)	$(this)当前被点击的对象
    var li = $(this);
    //设定被点击的li默认被选中,增加checked属性
    li.parent().find('a').removeClass('checked');
    li.find('a').addClass('checked');
    var data = {notebookId: li.data('notebookId')};
    //console.log(data);
    $.getJSON(url, data, function (result) {
        if (result.state == SUCCESS) {
            var notes = result.data.data;
            showNotes(notes)
        } else {
            alert(result.message);
        }
    });
}

//将笔记列表显示到屏幕上
function showNotes(notes) {
    //console.log(notes);
    //将每个笔记对象显示到屏幕
    var ul = $('#note-list ul');
    ul.empty();
    for (var i = 0; i < notes.length; i++) {
        var note = notes[i];
        var li = noteTemplate.replace('[title]', note.title);
        li = $(li);
        li.data('noteId', note.id);
        ul.append(li);
    }
    //默认选中第一个笔记，达到新建笔记的时候会自动选中新建的笔记
    $('#note-list').find('.note').eq(0).click();
}

//加载笔记本botebook  笔记本项目点击事件处理方法，加载全部笔记本
function loadNotebooks() {
    //利用ajax从服务器获取(get)数据
    var url = "getNote.do";
    var data = {userId: getCookie('userId')};
    $.getJSON(url, data, function (result) {
        console.log(result);
        // if(result.state==SUCCESS){
        // 	var notebooks = result.data;
        // 	//在showNotebooks方法中将全部的笔记本数据，显示到notebook-list区域。
        // 	showNotebooks(notebooks);
        // }
        if (result.state == 0) {
            var notebook = result.data.data;
            console.log(notebook);
            showNotebooks(notebook)
        }
        else {
            alert(result.message);
        }
    });

}

/** 在notebook-list区域中显示笔记本列表*/
function showNotebooks(notebook) {
    //找到显示笔记本列表的区域
    //遍历notebook数组，将每个对象创建一个li元素，添加到ul区域中。
    var ul = $('#notebook-list ul');
    ul.empty();
    for (var i = 0; i < notebook.length; i++) {
        var notebook = notebook[i];
        //将笔记名称显示
        var li = notebookTemplate.replace('[name]', notebook.name);
        //转成JS对象
        var li = $(li);
        //将笔记本notebook的id绑定到对应的li上面 data是Jquery提供的方法。
        li.data('notebookId', notebook.id);
        $(document).data(notebook.name, notebook.id);
        ul.append(li);
    }
}

//class="online notebook"等于绑定两个class
var notebookTemplate = '<li class="online notebook">' +
    '<a><i class="fa fa-book" title="online" rel="tooltip-bottom">' +
    '</i>[name]</a></li>';
var noteTemplate = '<li class="online note"><a>' +
    '<i class="fa fa-file-text-o" title="online" rel="tooltip-bottom"></i>[title]' +
    '<button type="button" class="btn btn-default btn-xs btn_position btn_slide_down  btn-note-menu">' +
    '<i class="fa fa-chevron-down"></i></button></a>' +
    '<div class="note_menu" tabindex="-1"><dl>' +
    '<dt><button type="button" class="btn btn-default btn-xs btn_move btn-note-move" title="移动至..."><i class="fa fa-random"></i></button></dt>' +
    '<dt><button type="button" class="btn btn-default btn-xs btn_share" title="分享"><i class="fa fa-sitemap"></i></button></dt>' +
    '<dt><button type="button" class="btn btn-default btn-xs btn_delete btn-note-delete" title="删除"><i class="fa fa-times"></i></button></dt>' +
    '</dl></div></li>';

