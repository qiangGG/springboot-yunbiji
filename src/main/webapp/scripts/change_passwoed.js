$(function () {

    $('.global').on('click', '#changePassword', changePassword);
    $('.global').on('click', '#back', back);
});

function changePassword() {
    var userId = getCookie("userId");
    var password = getCookie("password");
    var lastpassword = $('#last_password').val();
    if (lastpassword != password) {
        alert('请输入正确的原密码')
    }
    var new_password = $('#new_password').val();
    var data = { newpassword: new_password,userId: userId};
    $.ajax({
        url: 'changepwd.do',
        data: data,
        type: 'post',
        datatype: 'json',
        success: function (result) {
            if (result.data.state == 0) {
                alert("修改成功");
                window.location.href = 'login';
            }else {
                alert(result.data.message)
            }
        },
        error :function (e) {
          alert('发生未知错误');
        }
    });

}

//取消
function back() {
    window.location.href = "login"
}