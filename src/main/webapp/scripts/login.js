/*  scripts/login.js 编码为utf-8*/
$(function(){
	//console.log('wangqiang');
	$('#login').click(loginAction);
	$('#count').blur(checkName);
	$('#password').blur(checkPassword);
	
	//注册按钮
	$('#regist_button').click(registAction);
	$('#regist_username').blur(checkRegistName);
	$('#nickname').blur(checkNickName);
	$('#regist_password').blur(checkPwd);
	$('#final_password').blur(checkFinalPwd);
});
     function checkFinalPwd(){
    	 var fp=$('#final_password').val().trim();
    	 var rule=$('#regist_password').val().trim();
    	 if(rule && rule==fp){
    		 $('#final_password').next().hide();
    		 return true;
    	 }
    	 $('#final_password').next().show().find('span').html('密码不一致');
    	 return ;
     }
     function checkPwd(){
    	 var password=$('#regist_password').val().trim();
    	 var rule=/^\w{4,10}$/;
    	 if(rule.test(password)){
    		 $('#regist_password').next().hide();
    		 return true;
    	 }
    	 $('#regist_password').next().show().find('span').html('4~10个字符');
    	 return false;
     }
     
	function checkRegistName(){
		var name=$('#regist_username').val().trim();
		var rule=/^\w{4,10}$/;
		if(rule.test(name)){
			$('#regist_username').next().hide();
			return true;
		}
		$('#regist_username').next().show().find('span').html('4~10个字符');
		return false;
	}
	function checkNickName(){
		var name=$('#nickname').val().trim();
		var rule=/^\w{4,10}$/;
		if(rule.test(name)){
			$('#nickname').next().hide();
			
			return true;
		}
		$('#nickname').next().show().html('4~10个字符');
			return false;
	}
	
    function registAction(){

    	//检验界面参数
    	var n=checkRegistName()+checkPwd()+checkFinalPwd();
    	if(n!=3){
    		return;
    	}
      	//获取表单数据
    	var name=$('#regist_username').val().trim();
    	var nick=$('#nickname').val().trim();
    	var password=$('#regist_password').val().trim();
    	var confirm=$('#final_password').val().trim();
    	//发起AJAX请求
    	var url='regist';
    	var data={name:name,nick:nick,password:password,confirm:confirm};
    	
    	$.post(url,data,function(result){
    	      console.log(result);
    	      if(result.data.state==0){
    	    	  //返回登录页面
				  var date ="注册成功"
				  alert(date)
    	    	  $('#back').click();
    	    	  var name=data.name;
    	    	  $('#count').val(name);
    	    	  $('password').focus();//触发获得焦点事件
    	    	  $('#regist_username').val('');
    	    	  $('#regist_password').val('');
    	    	  $('#nickname').val('');
    	    	  $('#final_password').val('');
    	      }else if(result.data.state==1){
    	    	  $('#regist_username').next().show().find('span').html(result.data.message);
    	    	  
    	      }else if(result.state==3){
    	    	  $('#regist_password').next().show().find('span').html(result.message);
    	      }
    	});
    	}
    	//得到响应后,更新界面
  
    	/*var data={"name":name,"nick":nick,"password":password,"confirm":confirm};
    	$.ajax({
    		url:'user/regist.do',
    		data:data,
    		type:'post',
    		datatype:'json',
    		success:function(result){
    			console.log(result);
    			if(result.state==0){
    				//注册成功
    				var user=result.data;
    				alert(user);
    				//跳转
    				location.href="log_in.html";
    				
    			}
    			
    		},
    		error:function(e){
    			alert("注册失败");
    		}
    		
    		
    	});
    }*/
 function checkName(){
	 var name=$('#count').val();
	 var rule=/^\w{4,10}$/;
	 if(!rule.test(name)){
		 $('#count').next().html('4~10个字符');
		 return  false;
		 
	 }
	 $('#count').next().empty();
	 return true;
 }
 function checkPassword(){
	 var name=$('#password').val();
	 var rule=/^\w{4,10}$/;
	 if(!rule.test(name)){
		 $('#password').next().html('4~10个字符');
		 return  false;
		 
	 }
	 $('#password').next().empty();
	 return true;
 }
function loginAction(){
	//alert("loginAction");
	//获取用户输入的用户名和密码
	 var name=$('#count').val();
	 var password=$('#password').val();
	 var registusername=$('regist_username').val();
	 var n=checkName()+checkPassword();
	    if(n!=2){
	    	return;
	    }
	
	 //data对象中的属性名要与服务器控制器的参数名一致！ login(name,password)
	 var data={"name":name,"password":password};
	 $.ajax({
		 url:'login.do',
		 data:data,
		 type:'post',
		 datatype:'json',
		 success:function(result){
			 console.log(result);
			 if(result.data.state==0){

				 //登录成功
				 var user=result.data.data.id;
				 //登录成功以后将userId保存到cookie中
				 addCookie("userId",user);
				 addCookie("password",result.data.data.password);

				//跳转到edit.html

                 window.location.href="edit";
			 }else{
				 var msg=result.data.message;
				 if(result.state==1){
					 $('#count').next().html(msg);
				 }else if(result.state==3){
					 $('#password').next().html(msg);
				 }else{
					 alert(msg);
				 }

			 }
		 },
		 error:function(e){
			 alert("通信失败");
		 }
	 });
}