$(function () {
    //回收站
    $('#notebook-list').on('click', '#trash_button', deleteNotebook);
   //收藏笔记本
    $('#notebook-list').on('click','#like_button',likeNotebook);

});
//收藏笔记本
 function likeNotebook(){
     $('#can').load('./alert/alert_like.html',function () {
         $('.opacity_bg').show();
         $('.modal-footer').on('click','.sure',like);

     })
 }
 function like() {
     var notebookId =$('#notebook-list').find('a[class=checked]').parent().data('notebookId');
     if (notebookId==null){
         alert('请选择一个笔记本');
         return false;
     }
     alert("笔记本Id: "+notebookId)
 }
//回收站
function deleteNotebook() {
    var notebookId = $('#notebook-list').find('a[class=checked]').parent().data('notebookId');
    if (notebookId == null) {
        alert('请选择要删除的笔记本');
        return false;
    }
    $('#can').load('../alert/alert_delete_notebook.html', function () {
        $('.opacity_bg').show();

        $('.modal-footer').on('click','.sure',delnotebook)

    });

    function delnotebook() {
        var notebookId = $('#notebook-list').find('a[class=checked]').parent().data('notebookId');
        var url = 'del.do';
        var data = {notebookId:notebookId};
        $.post(url,data,function (result) {
          if(result.data.state==0){
          // var li=$('#notebook-list').find('a[class=checked]').parent();
          // li.click();
              alert("DELETE SUCCESS")
          }else {
              alert(result.data.message)
          }
        })
    }
}
