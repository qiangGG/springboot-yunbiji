//scripts/note.js 编码一定是 utf-8
var SUCCESS=0;
var ERROR=1;
$(function(){
	 var userId=getCookie('userId');
	 	console.log(userId);
	//网页加载以后,立即读取笔记本列表
	 loadNotebooks();
	 //绑定笔记本列表区域的点击事件  
	 //on()方法绑定可以区分事件源
	 //click()方法绑定，无法区分事件源
	 $('#notebook-list').on('click','.notebook',loadNotes);
	 $('#note-list').on('click','#add_note',showAddNoteDiglog);
	 $('#can').on('click','.create-note',showAddNoteDiglog);
	 $('#can').on('click','.close,.cancel',closeDialog)
	 $('#note-list').on('click','.note',loadNote)
	 $('#note-list').on('click', '#add_note', showAddNoteDialog);
	 $('#can').on('click','.close,.cancel',closeDialog)
	 $('#note-list').on('click','.btn-note-menu',showNoteMenu);
	 $(document).click(hideNoteMenu);
     $('#note-list').on('click','.btn-note-move',showMoveNoteDialog);
	 $('#can').on('click','.move-note',moveNote);
	 
});

function moveNote(){
	  var url='notebook/move.do';
	  var id=$(document).data('note').id;
  var bookId=$('#moveSelect').val();
  if(bookId==$(document).data('notebookId')){
		  return;
	  }
  var data={noteId:id,notebookId:bookId};
	  $(url,data,function(result){
		  if(result.state==SUCCESS){
			  var li=$('#note-list.checked').parent();
		  var lis=li.siblings();
			  if(lis.size()>0){
			  lis.eq(0).click();
			  }else{
				  $('#input_note_title').val("");
				  um.setContent("");
		  }
			  li.remove();
		  closeDiglog();//关闭对话框
		  }else{
		  alert(result.message);
	  }
	  });
  }

  function showMoveNoteDialog(){
	
	
	  $('#can').load('alert/alert_move.html',loadNotebookOptions);
	  $('.opacity_bg').show();
		
  }
	 /* alert('必须选择笔记!');*/
 function loadNotebookOptions(){
	  // var url='notebook/list.do';
	  var data={userId:getCookie('userId')};
	  $.getJSON(url,data,function(result){
		   if(result.state==SUCCESS){
			   var notebooks=result.data;
		   $('#moveSelect').empty();
		   var id=$(document).data('notebookId');
		   for(var i=0;i<notebooks.length;i++){
			   var notebook=notebooks[i];
			   var opt=$('<option></option>').val(notebook.id).
				   html(notebook.name);
				   //默认选定当时笔记的笔记本ID
				   if(notebook.id==id){
				   opt.attr('selected','selected');
			   }
				   $('#moveSelect').append(opt);
		   }
			   
		   }  else{
			   alert(result.message);
		   }                
	  });
	  
  }
function hideNoteMenu(){
	$('.note_menu').hide();
}
 function showNoteMenu(){
	 //找到菜单对象,调用show()方法
	 
	 var btn=$(this);
	 //如果是当前被选定的笔记项目,就弹出子菜单
	// $('.note_menu').hide(function(){
		 btn.parent('.checked').next().toggle();
		 // btn.parent('.checked')获取当前按钮的父元素,这个元素必须符合选择器'.checked',如果不符合就返回空的JQuery元素
	// });
	
	return false;//阻止点击事件的继续传播
 }
function closeDialog(){
    $('.opacity_bg').hide();
    $('#can').empty();
}

function showAddNoteDialog(){
    var id = $(docuemnt).data('notebookId');
    if(id){
        $('#can').load('alert/alert_note.html', function(){
            $('#input_note').focus();
        });
        $('.opacity_bg').show();
    }
    alert('必须选择笔记本!');
}
function loadNote(){
	
	var li=$(this);
	li.parent().find('a').removeClass('checked');
	li.find('a').addClass('checked');
	var url='note/load.do';
	var data={noteId:li.data('noteId')};
	console.log(data);
	$.getJSON(url,data,function(result){
		console.log(result);
		if(result.state==SUCCESS){
			var note=result.data;
			showNote(note);
			
		}else{
			alert(result.message)
		}
		
	});
	 
}
function showNote(note){
	

	  $('#input_note_title').val(note.title);
	  um.setContent(note.body);
	  //绑定笔记信息,用于保存操作
	  $(document).data('note',note);
}


  function closeDialog(){
	  $('.opacity_bg').hide();
	  $('#can').empty();
  }
  
function showAddNoteDiglog(){
	$('#can').load('alert/alert_note.html',function(){
		
		$('#input_note').focus();
	});
	$('.opacity_bg').show();
}
 /** 笔记本项目点击处理方法,加载全部笔记 **/
   function loadNotes(){
	   
	   //关闭回收站 
	   var li=$(this);//当前被点击的对象li
	   //在被点击的笔记本li增加选定效果
	   li.parent().find('a').removeClass('checked');//移除选定效果
	   li.find('a').addClass('checked');
	   var url='note/note.do';
	   var data={notebookId:li.data('notebookId')};
	   $(document).data('notebookId', li.data('notebookId'));
	   console.log(data);
	$.getJSON(url,data,function(result){
		 if(result.state==SUCCESS){
			 var notes=result.data;
			
			 showNotes(notes);
		 }else{
			 alert(result.message);
		 }
		
	});
   }
   
   
   /** 将笔记本列表信息显示到屏幕上**/
   function showNotes(notes){
	   console.log(notes);
	   //将每个笔记显示到屏幕的区域
	  var ul= $('#note-list ul');
	  ul.empty();
	  var noteTemplate='<li class="online note">'+
		'<a>'+
		'<i class="fa fa-file-text-o" title="online" rel="tooltip-bottom"></i> [title]<button type="button" class="btn btn-default btn-xs btn_position btn_slide_down btn-note-menu"><i class="fa fa-chevron-down"></i></button>'+
	'</a>'+
	'<div class="note_menu" tabindex="-1">'+
		'<dl>'+
			'<dt><button type="button" class="btn btn-default btn-xs btn_move btn-note-move" title="移动至..."><i class="fa fa-random"></i></button></dt>'+
			'<dt><button type="button" class="btn btn-default btn-xs btn_share" title="分享"><i class="fa fa-sitemap"></i></button></dt>'+
			'<dt><button type="button" class="btn btn-default btn-xs btn_delete" title="删除"><i class="fa fa-times"></i></button></dt>'+
		'</dl>'+
	'</div>'+
'</li>';
	  for(var i=0;i<notes.length;i++){
		  var note=notes[i];
		  var li=noteTemplate.replace('[title]',note.title);
		  li=$(li);
		  li.data("noteId",note.id);
		  ul.append(li);
	  }
		  
	  }
	
	


   /* 加载笔记本列表数据*/
function loadNotebooks(){
	//利用ajax服务器获取(get)数据
	// var url='notebook/list.do';
	var data={userId:getCookie('userId'),name:'demo'};
	// $.getJSON(url,data,function(result){
	// 	console.log(result);
	// 	if(result.state==SUCCESS){
	// 		var notebooks=result.data;
	// 		//在showNotebooks方法中将全部的笔记本数据notebooks显示到notebook-list区域
	// 		showNotebooks(notebooks);
	// 	}else{
	// 		alert(result.message);
	// 	}
	//
	// });
	//
}
/**
 * 
 *  notebooks-list区域显示笔记本列表
 */
function showNotebooks(notebooks){
	//找到显示笔记本列表的区域 遍历notebooks数组,将为每个对象创建一个li元素,添加到ul元素中
	 var ul=$('#notebook-list ul');
	 ul.empty();
	 for(var i=0;i<notebooks.length;i++){
		 var notebook=notebooks[i];
		 var li=notebookTemplate.replace('[name]',notebook.name);
		 var li=$(li);
		 //将notebook.id绑定到li 
		 li.data("notebookId",notebook.id);
		 
		 ul.append(li);
	 }
}
	var notebookTemplate='<li class="online notebook">'+
							'<a><i class="fa fa-book" title="online" rel="tooltip-bottom"></i>[name]</a></li>';
							
								
									 
