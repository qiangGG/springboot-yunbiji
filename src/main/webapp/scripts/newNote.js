$(function () {
    console.log("wellCome.cloudbook.js");
    //加载笔记
    loadNotebooks();
    //新建笔记
    $('#add_note').click(createNewNote);
    //新建笔记本
    $('#add_notebook').click(CreateNewNotebook);
    //保存笔记
    $('#save_note').click(saveNote);
    //关闭弹窗
    $('#can').on('click', '.close,.cancel,.create-note', closeAlert_note);
    //单击笔记本
    $('#notebook-list').on('click', '.notebook', shownotebook);
    //显示操作笔记窗口
    $('#note-list').on('click', '.btn-note-menu', showNoteMenu);
    $(document).click(hideNoteMenu);
    //单击笔记，显示 title 和内容
    $('#note-list').on('click', '.note', showNoteList);
    //删除功能
    $('#note-list').on('click', '.btn-note-delete', deleteNote);


});

//新建笔记
function createNewNote() {
    $('#can').load('./alert/alert_note.html', function () {
        $('.opacity_bg').show();
        $('#input_note').focus();
        $('.create-note').click(createNote);

    })
}

function createNote() {
    var notebookId = $('#notebook-list').find('a[class=checked]').parent().data('notebookId');
    if (notebookId == null) {
        alert('请选择笔记本');
        return false;
    }
    var userId = getCookie('userId');
    var title = $('#input_note').val();
    var url = 'createNewNote.do';
    var data = {notebookId: notebookId, userId: userId, title: title};
    $.post(url, data, function (result) {
        if (result.data.state == 0) {
            alert('创建成功');
            var li = $('#notebook-list').find('a[class=checked]').parent();
            li.click();
        } else {
            alert(result.data.message)
        }
    })
}

//逻辑删除功能
function deleteSure() {
    var noteId = $('#note-list').find('a[class=checked]').parent().data('noteId');
    var data = {noteId: noteId};
    var url = 'deleteNote.do';
    $.getJSON(url, data, function (result) {
        if (result.data.state == 0) {
            alert("删除成功");
            //刷新整个页面
            window.location.reload();
            // var li =$('#notebook-list').find('a[class=checked]').parent();
            //  li.click();
        } else {
            alert(result.data.message)
        }
    });

}

//删除笔记
function deleteNote() {
    $('#can').load('./alert/alert_delete_note.html', function () {
        $('.opacity_bg').show();
        $('#modalBasic_7').on('click', '.sure', deleteSure);


    });

}

//根据点击笔记title，展现笔记标题和内容
function showNoteList() {
    //当前点击得笔记本
    var li = $(this);
    //li 得父元素找到 a,移除’checked'类
    li.parent().find('a').removeClass('checked');
    //li得 父元素找到 a ,增加checked 类
    li.find('a').addClass('checked');
    //获得当前点击笔记本得noteId
    var noteId = $('#note-list').find('a[class="checked"]').parent().data('noteId');
    showCloudNote(noteId);
}

function showCloudNote(noteId) {
    var url = 'getCloudNote.do';
    var data = {noteId: noteId};
    $.post(url, data, function (result) {
        if (result.data.state == 0) {
            console.log(result);
            var title = result.data.data.title;
            var body = result.data.data.body;
            console.log("noteId:" + noteId);
            //显示笔记标题
            document.getElementById('input_note_title').value = title;
            //显示笔记内容
            if (body == null) {
                um.setContent('');
                return false;
            }
            um.setContent(body)

        } else {
            alert(result.data.data.message);
        }
    })
}

//显示与掩藏操作笔记窗口
function showNoteMenu() {
    console.log('note-menu');
    //找到菜单调方法
    var btn = $(this);
    //只有被选定的当前笔记项目才可以调出子菜单
    //btn.parent('.checked')获取当前按钮的父元素，在父元素满足条件的情况下
    //才返回Jquery对象，调用Jquery的方法，否则就返回空的Jquery元素。
    btn.parent('.checked').next().toggle();
    //阻止点击事件的继续传播，在方法中返回false
    return false;


}


function hideNoteMenu() {
    $('.note_menu').hide();
}

//单击笔记本,显示笔记本下的笔记
function shownotebook() {
    var li = $(this);
    //设定被点击的li默认被选中,增加checked属性
    li.parent().find('a').removeClass('checked');
    li.find('a').addClass('checked');

    var notebookId = $('#notebook-list').find('a[class="checked"]').parent().data('notebookId');
    console.log("notebookId :" + notebookId);
    addCookie('notebookId', notebookId);

    var url = 'findNote.do';
    var data = {notebookId: notebookId};
    $.post(url, data, function (result) {
        console.log(result);
        if (result.data.state == 0) {
            var notes = result.data.data;
            console.log(notes);
            showNoteName(notes);

        } else {
            //如果笔记本下没有笔记, 就清除上一次显示的笔记title和body，，显示空白
            $('#input_note_title').val('');
            um.setContent('');
            var ul = $('#note-list ul');
            ul.empty();
        }

    })
}

//显示笔记title
function showNoteName(notes) {
    console.log("showNoteName");
    var ul = $('#note-list ul');
    ul.empty();
    for (var i = 0; i < notes.length; i++) {
        var note = notes[i];
        var li = noteTemplate.replace('[title]', note.title);
        li = $(li);
        //将笔记本note的id绑定到对应的li上面 data是Jquery提供的方法。
        li.data("noteId", note.id);
        console.log(li.data("noteId"));
        ul.append(li);

    }

}

//创建笔记本
function CreateNote() {
    var userId = getCookie('userId');
    var noteName = $('#input_notebook').val();
    console.log(noteName);
    console.log(userId);
    var data = {userId: userId, noteName: noteName};
    var url = 'createNote.do';
    $.post(url, data, function (result) {
        if (result.data.state == 0) {
            alert("创建成功");
            //创建成功重新加载笔记本列表
            loadNotebooks();
        } else {
            alert(result.data.message)
        }
    });

}

//弹出新建笔记本窗口
function CreateNewNotebook() {

    $('#can').load('./alert/alert_notebook.html', function () {
        //显示背景颜色
        $('.opacity_bg').show();
        //焦点
        $('#input_notebook').focus();
        //创建笔记本事件
        $('.sure').click(CreateNote);

    })
}

//关闭弹窗
function closeAlert_note() {
    $('.opacity_bg').hide();
    $('#can').empty();//移除元素中的内容
    //关闭弹窗的同时加载笔记本列表
    loadNotebooks();

}

//展示笔记内容
function showNotes() {
    console.log("点击了");
    var li = $(this);
    //设定被点击的li默认被选中,增加checked属性
    li.parent().find('a').removeClass('checked');
    li.find('a').addClass('checked');
    var data = {notebookId: li.data('notebookId')};

    console.log(data);
}

//网页加载以后立即读取笔记本列表
function loadNotebooks() {

    var userId = getCookie("userId");
    var data = {userId: userId};
    var url = 'getNote.do';
    $.post(url, data, function (result) {
        if (result.state == 0) {
            var notebook = result.data.data;
            console.log(notebook);
            showNotebooks(notebook)
        }
    });

}

//展示笔记本
function showNotebooks(notebooks) {
    var ul = $('#notebook-list ul');
    ul.empty();
    for (var i = 0; i < notebooks.length; i++) {
        var notebook = notebooks[i];
        console.log("name" + notebook.name);
        console.log("id" + notebook.id);
        //显示笔记名称
        var li = notebookTemplate.replace('[name]', notebook.name);
        //转成JS对象
        li = $(li);
        //将笔记本notebook的id绑定到对应的li上面 data是Jquery提供的方法。
        li.data('notebookId', notebook.id);
        $(document).data(notebook.name, notebook.id);
        ul.append(li);
    }
    //  $('#notebook-list').find('.notebook').eq(0).click();
}

//保存笔记
// function saveNote() {
//     //获得当前点击笔记本得noteId
//     var noteId = $('#note-list').find('a[class="checked"]').parent().data('noteId');
//     console.log("saveNote");
//     var notebookId = getCookie('notebookId');
//     console.log("saveNote noteId" + noteId);
//     var title = $('#input_note_title').val();
//     var body = um.getContent();
//     //格式化内容,去除标签符号
//     var regex = /(<([^>]+)>)/ig;
//     var b = body.replace(regex, '');
//     console.log("b:" + b);
//     var userId = getCookie("userId");
//     console.log(title);
//     //获取原来的title和body
//     var oldTitle = $(document).data('oldTitle');
//     var ob = $(document).data('oldBody');
//     if (ob != null) {
//         var oldBody = ob.replace(regex, '');
//         console.log("ob:" + oldBody);
//     }
//     if (oldTitle == "undefined" && oldBody == "undefined") {
//         alert('请选择笔记本');
//         return false;
//     }
//
//     if (title == oldTitle && b != oldBody) {
//
//         console.log('更新内容');
//         console.log("旧内容:" + oldBody);
//         console.log("新内容:" + b);
//         oldTitle = title;
//         oldBody = b;
//         //更改内容，标题不变
//         var url = 'updateNote.do';
//         var data = {title: title, body: body, noteId: noteId};
//         $.getJSON(url, data, function (result) {
//             if (result.data.state == 0) {
//                 alert("更新内容成功");
//                 //刷新笔记本列表
//                 var li = $('#notebook-list').find('a[class=checked]').parent();
//                 li.click();
//             } else {
//                 alert(result.data.message)
//             }
//             return false;
//         })
//     }
//     if (title != oldTitle && b == oldBody) {
//
//         console.log("修改标题");
//         console.log('旧标题:' + oldTitle);
//         console.log('新标题:' + title);
//
//         //更改标题,内容不变
//         var url = 'renameNote.do';
//         var data = {title: title, body: oldBody, noteId: noteId};
//         console.log('title:' + title);
//         $.post(url, data, function (result) {
//             if (result.data.state == 0) {
//                 alert("修改标题成功");
//                 //刷新笔记本列表
//
//                 var li = $('#notebook-list').find('a[class=checked]').parent();
//                 console.log(li);
//                 li.click();
//             } else {
//                 alert(result.data.message)
//             }
//             return false;
//         })
//     }
//     //新标题和新内容,增加笔记
//     if (title != oldTitle && b != oldBody) {
//         console.log('新建笔记');
//         console.log("旧oldTitle:" + oldTitle);
//         console.log("旧oldBody:" + oldBody);
//         console.log("新title:" + title);
//         console.log("新body:" + b);
//         var url = 'saveNote.do';
//         var data = {title: title, body: b, userId: userId, notebookId: notebookId};
//         console.log("新title:" + title);
//         console.log('新内容:' + b);
//         $.post(url, data, function (result) {
//             if (result.state == 0) {
//                 alert("新建笔记成功");
//                 //刷新笔记本列表
//                 var li = $('#notebook-list').find('a[class=checked]').parent();
//                 li.click();
//             }
//         });
//     }
//
//
// }
//

function saveNote() {
    //获得当前点击笔记本得noteId
    var noteId = $('#note-list').find('a[class="checked"]').parent().data('noteId');
    if (noteId == null) {
        alert("请选择一个笔记")
    }
    console.log("saveNote");
    var notebookId = getCookie('notebookId');
    console.log("saveNote noteId" + noteId);
    var title = $('#input_note_title').val();
    var body = um.getContent();
    //格式化内容,去除标签符号
    var regex = /(<([^>]+)>)/ig;
    var b = body.replace(regex, '');
    console.log("b:" + b);
    var userId = getCookie("userId");
    var data = {body: b, title: title, noteId: noteId};
    var url = 'updateNote.do';
    $.post(url, data, function (result) {
        if (result.data.state == 0) {
            alert('保存成功');
            var li = $('#notebook-list').find('a[class=checked]').parent();
            li.click();
        } else {
            alert(result.data.message);
        }

    })
}

//class="online notebook"等于绑定两个class
var notebookTemplate = '<li class="online notebook">' +
    '<a><i class="fa fa-book" title="online" rel="tooltip-bottom">' +
    '</i>[name]</a></li>';
var noteTemplate = '<li class="online note"><a>' +
    '<i class="fa fa-file-text-o" title="online" rel="tooltip-bottom"></i>[title]' +
    '<button type="button" class="btn btn-default btn-xs btn_position btn_slide_down  btn-note-menu">' +
    '<i class="fa fa-chevron-down"></i></button></a>' +
    '<div class="note_menu" tabindex="-1"><dl>' +
    '<dt><button type="button" class="btn btn-default btn-xs btn_move btn-note-move" title="移动至..."><i class="fa fa-random"></i></button></dt>' +
    '<dt><button type="button" class="btn btn-default btn-xs btn_share" title="分享"><i class="fa fa-sitemap"></i></button></dt>' +
    '<dt><button type="button" class="btn btn-default btn-xs btn_delete btn-note-delete" title="删除"><i class="fa fa-times"></i></button></dt>' +
    '</dl></div></li>';